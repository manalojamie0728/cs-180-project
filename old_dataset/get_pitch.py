import midi
pattern = midi.read_midifile('12DaysOfChristmas.mid')
Fo = open("pitch_out.txt", "w")

for track in pattern:
    for event in track:
        if isinstance(event, midi.NoteEvent):
            Fo.write(str(event.get_pitch())+"\n")

Fo.close()
