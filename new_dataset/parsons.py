import midi
import os
import sys
import editdistance
from difflib import SequenceMatcher as SM

f_in_song=open("song_dataset_list.txt","r")
f_in_index=open("song_index.txt","r")
song_list = [str(x) for x in f_in_song]
song_index = [int(x) for x in f_in_index] #index and correct classification ng song

def MakeIntPitch(filename):
	pitch_str=[]
	pattern = midi.read_midifile(filename)
	for track in pattern:
		for event in track:
			if isinstance(event, midi.NoteEvent): # check that the current event is a NoteEvent, otherwise it won't have the method get_pitch() and we'll get an error
				pitch_str.append(event.get_pitch())
	return pitch_str

def MakeParsers(pitch_str):
	temp= ""
	x = pitch_str[0]
	for i in range(2, len(pitch_str), 2):
		if x < pitch_str[i]:
			temp += "U"
		elif x > pitch_str[i]:
			temp += "D"
		else:
			temp += "R"
		x = pitch_str[i]
	return temp

'''For Training'''
song_database = []
for x in range(0,len(song_index)):
	pitch_str = MakeIntPitch("songs/"+str(x)+".mid")	#yung mga integers pa lang
	temp = MakeParsers(pitch_str) #parsons code per song na iaappend sa database
	song_database.append(temp)

'''For Testing'''
scores = []
os.system("python audio_to_midi_melodia.py rudolph2.wav rudolph2.mid 60 --smooth 0 --minduration 0.1 --jams")
test_pitch = MakeIntPitch("rudolph2.mid") #template pa lang
test_parsons = MakeParsers(test_pitch)

for x in range(0,len(song_database)):
	#do string matching here
	#song_score=editdistance.eval(song_database[x],test_parsons)/float(len(song_database[x]))
	song_score = SM(None, test_parsons, song_database[x]).ratio()
	scores.append(song_score)
	song_index.append(x)

'''Printing Results'''
scores, song_index = zip(*sorted(zip(scores, song_index)))	#sinosort niya yung scores and corresponding index nung song
top5_index=[]
top=1
top_cnt=0
print "Here are the Top 5 Songs that matched your query: "
while(top_cnt!=5):
	for x in song_index:
		#prevent song duplication in results
		if x not in top5_index:
			top5_index.append(x)
			top_cnt+=1
		if top_cnt == 5:
			break

#imamatch na yung top 5 indeces sa corresponding song name
for x in top5_index:
	song = song_list[x].split()
	print str(top)+": "+" ".join(song)
	top+=1

'''Added Feature'''
#add recorded voice to database
choice = raw_input("Help us expand our database by choosing the correct classification of the song? y/n\nChoice: ")
if str.lower(choice) == "y":
	choice2 = 0
	while(choice2 != 2):
		choice2 = input("Choose [1] Check list of songs [2] Give the corresponding index to the song\nChoice: ")
		if choice2 == 1:
			os.system("gedit song_list.txt")

	#appends index of correct classification to list of indeces
	f_out = open("song_index.txt","a")
	index = input("Song index: ")
	f_out.write(str(index))
	#renaming of voice recording to the current number of songs in database 
	#because file reading is based on integer naming
	rename_voice_file = "mv temp.mid "+str(len(song_index))+".mid"
	os.system(rename_voice_file)
	#move record to songs folder
	os.system("mv "+str(len(song_index))+".mid songs")
	f_out.close()

f_in_index.close()
f_in_song.close()
