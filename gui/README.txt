Song Recognition Project for CS 180: Artificial Intelligence by
Ablog, Jennie Ron
Del Castillo, Mary Abigail
Manalo, Juan Miguel

I. DATASET
The dataset are inside the "songs" folder. The original dataset are the files from 0.mid to 53.mid (total of 54 songs). The other midi files are the previous recordings which are added to the dataset.

II. LIST OF SONGS
The list of available songs in the dataset are contained in "song_list.txt".

III. REQUIREMENTS/INSTALLATION INSTRUCTIONS
Requirements:
1) Python 2.7
2) Linux PC

Libraries installation:
1) Python midi:
   git clone https://github.com/vishnubob/python-midi.git
   OR
   copy paste "python-midi-master" from ZIP_files folder into your Desktop.

2) Melodia:
   a) Download zip here http://mtg.upf.edu/technologies/melodia?p=Download%20and%20installation
   OR
   get "MTG-MELODIA 1.0 (Linux 64-bit).zip" file from ZIP_files folder included here
   b) Make a new folder in "Home" directory and name it "vamp".
   c) Extract zip file in the newly made "vamp" folder.

3) Audio to midi Melodia
   a) git clone https://github.com/justinsalamon/audio_to_midi_melodia or get "audio_to_midi_melodia-master.zip" file from ZIP_files folder
   b) extract the files from the "audio_to_midi_melodia-master.zip" in the same folder as the "gui.py". If the "audio_to_midi_melodia.py","_init_.py","_init_.pyc" are already present in the folder, no need to do this step.
   to use this:
   a) install these libraries:
      sudo pip install librosa
      sudo pip install vamp
      sudo pip install MIDIUtil
      sudo pip install jams

4) PyQt4 (GUI)
   sudo apt-get install python-qt4.

5) arecord (voice recorder)
   Usually default program in linux distributions but if it's not yet installed then you can use this command.
   sudo apt-get install audio-recorder


IV.HOW TO USE THE PROGRAM "NAME THAT TUNE":
1) Go to the same directory where gui.py is located.
2) Open your terminal in this directory.
3) Type "python gui.py"
4) Select "Record your query" if you want to start recording your voice.
4.1) Press Ctrl+C if you want to stop recording and then just wait for the results to show on the window.
5) Select "Add MIDI to database" if you want your recording to be added to the existing database
5.1) Choose "y" if you want to help expand the database.
5.2) Then choose [1] to check the #/no. of the song in the song list.
5.3) Close the txt file if you have already checked the #/no. to proceed.
5.4) Choose [2] to give the correct #/no. of the song
5.5) Input the correct #/no. then enter
6) Select "Upload MIDI query" if you already have a previously recorded MIDI file that you want to classify. Note that this will only work if the recorded file is of MIDI format or having ".mid" file extension.
7) Select "Play a MIDI file" if you want to hear a song from the dataset. Just choose the MIDI file you want to play and the program will play it.
7.1) Ctrl+C again if you want to stop listening.

Reference/s:
http://www.justinsalamon.com/news/melody-extraction-in-python-with-melodia
http://www.justinsalamon.com/news/category/melody%20extraction