import pyaudio
import wave
import os
import sys
from parsons import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class Example(QWidget):
    
    def __init__(self):
        super(Example, self).__init__()
        
        self.initUI()
        
        
    def initUI(self):
        
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.black)
        self.setPalette(p)

        self.setGeometry(300, 300, 500, 500)
        self.setWindowTitle('TENENENEN TENEN!')
        self.setWindowIcon(QIcon('web.png'))        

        recordButton = QPushButton('RECORD', self)
        recordButton.setToolTip('Sing the tune you can\'t name')
        recordButton.resize(recordButton.sizeHint())
        recordButton.move(210, 100)
        recordButton.clicked.connect(recordButton_clicked)

        inputMidi = QPushButton('UPLOAD', self)
        inputMidi.setToolTip('Upload MIDI file to be guessed')
        inputMidi.resize(inputMidi.sizeHint())
        inputMidi.move(210, 150)
        inputMidi.clicked.connect(inputMidi_clicked)

        addMidi = QPushButton('ADD', self)
        addMidi.setToolTip('Add MIDI to database')
        addMidi.resize(addMidi.sizeHint())
        addMidi.move(210, 200)
        addMidi.clicked.connect(addMidi_clicked)

        playMidi = QPushButton('PLAY', self)
        playMidi.setToolTip('Play MIDI')
        playMidi.resize(playMidi.sizeHint())
        playMidi.move(210, 250)
        playMidi.clicked.connect(playMidi_clicked)
    
        self.show()

def main():
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())

def recordButton_clicked():
    os.system('arecord temp.wav')

def inputMidi_clicked():
    filename = QFileDialog.getOpenFileName()
    print 'Uploaded!'

def addMidi_clicked():
    filename = QFileDialog.getOpenFileName()
    print 'Uploaded!'

def playMidi_clicked():
    filename = QFileDialog.getOpenFileName()
    os.system(str('timidity '+filename))

if __name__ == '__main__':
    main()